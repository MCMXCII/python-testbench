import sys

class calculator:
    symbols = {
        'add': '+',
        'subtract': "-",
        'multiple': '*',
        'divide': '/',
        'power':'^'
    }
    def __init__(self):
        if len(sys.argv) > 1:
            equation = sys.argv[1]
            if len(equation) > 1:
                for s in self.symbols:
                    tmp = equation.split(self.symbols[s])
                    if len(tmp) > 1:
                        method = getattr(self, s)
                        self.output(equation + " = " + str(method(int(tmp[0]), int(tmp[1]))))
            else:
                self.error("Please provide an equation")
        else:
            self.error("Please provide an equation")
    def add(self, a, b):
        return a + b
    def subtract(self, a, b):
        return a - b
    def multiple(self, a, b):
        return a * b
    def divide(self, a, b):
        return a / b
    def power(self, a, b):
        return a ** b
    def error(self, message):
        print(message)
    def output(self, message):
        print(message)
