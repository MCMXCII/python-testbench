import sys, optparse

class CLI:
    versionString = "0.0.0"
    def __init__(self):
        self.parser = optparse.OptionParser(version=self.versionString)
        self.parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Make lots of noise [default]")
        self.addParams()
        (options, args) = self.parser.parse_args()
        if len(sys.argv) > 1:
            if options.verbose:
                getattr(self, sys.argv[1])(options)
            else:
                try:
                    getattr(self, sys.argv[1])(options)
                except:
                    print("Unknown command: " + sys.argv[1])
        else:
            self.parser.print_help()
    def addParams(self):
        pass