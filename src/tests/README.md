# Test Use Cases  
## CLI  
`cli` / `cli -h` / `cli --help` - Should return help dialog  
`cli run` - Should run `run` command, outputting default params  
`cli run --add abc` - Should run `run` command, outputting default params with `add` updated to `abc`  
`cli --version` - Should return versionString property  
  
## Calculator  
`calculator "1 + 2"` - Should return "1 + 2 = 3"  
`calculator "1 - 2"` - Should return "1 - 2 = -1"  
`calculator "1 * 2"` - Should return "1 * 2 = 2"  
`calculator "1 / 2"` - Should return "1 / 2 = 0.5"  
`calculator "1 ^ 2"` - Should return "1 ^ 2 = 1"  
