import sys
sys.path.append("..")

from libraries.cli import CLI

class Test(CLI):
    versionString = "Test App 1.0.0"
    def addParams(self):
        self.parser.add_option("-a", "--add", action="store", dest="add", help="Add value")
    def run(self, args):
        print(args)

Test()